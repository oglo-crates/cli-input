mod macros {
    #![allow(unused_macros)]
    #![allow(unused_imports)]
    #![allow(dead_code)]
    
    #[macro_export]
    /// Prompt the user for some input.
    ///
    /// # Examples
    /// ```rust,ignore
    /// use cli_input::prelude::*;
    ///
    /// let name: String = input!("What is your name?: ");
    /// ```
    macro_rules! input {
        ($($arg:tt)*) => {
            {
                input(&format!($($arg)*))
            }
        };
    }
    
    #[macro_export]
    /// Prompt the user for some input, and hide the characters being typed out.
    ///
    /// # Examples
    /// ```rust,ignore
    /// use cli_input::prelude::*;
    ///
    /// let password: String = input_hidden!("What is your password?: ");
    /// ```
    macro_rules! input_hidden {
        ($($arg:tt)*) => {
            {
                input_hidden(&format!($($arg)*))
            }
        };
    }
    
    pub use input;
    pub use input_hidden;
}

mod functions {
    use std::io::Write;
    use rpassword;

    pub fn input(prefix: &str) -> String {
        let mut prompt = String::new();
    
        print!("{}", prefix);
    
        std::io::stdout().flush().unwrap();
        std::io::stdin().read_line(&mut prompt).unwrap();
    
        return prompt.trim().to_string();
    }

    pub fn input_hidden(prefix: &str) -> String {
        let prompt = rpassword::prompt_password(prefix).unwrap();
    
        return prompt.trim().to_string();
    }
}

pub mod prelude {
    pub use super::macros::*;
    pub use super::functions::*;
}
