# CLI Input

A crate filled with various functions and macros for collecting user input in the terminal.

# Examples:
```rust
use cli_input::prelude::*;

let name: String = input!("Name: ");
let password: String = input_hidden!("Password: ");

println!("\n==== Information ====");
println!("  Name: {}", name);
println!("  Password: {}", password);
```
